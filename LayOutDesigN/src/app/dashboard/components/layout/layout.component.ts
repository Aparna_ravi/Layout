import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService } from '../../../register.service';
import { AuthService } from '../../../auth/auth.service';
import { NgForm, FormBuilder, FormControl, FormGroup, Validator } from '@angular/forms';
//  import  {IFile} from '../../../models/Fileup;
import  IFile from '../../../models/Fileup'
import { CookieService } from '../../../../../node_modules/ngx-cookie-service';

interface Iobj {
  string: FormControl
}

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  // public show:boolean = false;
  // public buttonName:any = 'Show';
  public show: boolean = false;
  public buttonName: any = 'Show';
  public Show: boolean = true;
  public hidden: boolean = false;
  public show1: boolean = true;
  public show2: boolean = true;
  public show3: boolean = true;
  public show4: boolean = true;
  public imgshow: boolean = false;
  public imgshow1: boolean = true;
  public imgshow2: boolean = false;
  public isActive: boolean = false;
  public isActive1: boolean = false;


  public grid2: boolean = false;
  public grid3: boolean = true;
  metdat: any;
  lists: any;
  list1: Array<String>
  filters: Array<String>
  filtername: Array<String>
  chkb: Array<String>
  chkbimage: any;
  rt: any;
  imgval: any;
  imgname: any;
  ser = "";
  
  // a.myurl = new URL("http://www.google.ch");
  filt: Array<String> = [];


  myFrom = new FormGroup({
    name: new FormControl('')
  });

 


  gridList() {
    this.grid2 = true;
    this.grid3 = false;
    this.isActive=true;
    this.isActive1=false;
  }
 path:string;
  gridList2() {

    this.grid3 = true;
    this.grid2 = false;
    this.isActive=false;
    this.isActive1=true;
  }
 
  constructor(private router: Router, private registerservice: RegisterService, private auth: AuthService,private cook:CookieService) {

// if(this.auth.isLoggedIn())
// {
//   this.getmettaa();
//   this.lists = this.auth.namedata;
// console.log("aparna"+this.lists);

// this.list1 = this.auth.test1;

   
// var n=this.lists.length
// // var n=3
// var k = 0, p = 0;
// for (var i = 0; i < n; i++) {
//   this.list1[k++] = this.lists[i].name;
// }
// this.filters = [...this.list1];
// this.filtername = [...this.list1];

// }

// else
// {
// console.log("inside else")
// }
// this.lists = auth.namedata;
this.list1 = auth.test1;
// console.log("lists"+this.lists)

// this.path="G:\LayOutDesigN\src\"

// this.path="abc"








this.registerservice.getmetadata().subscribe(a => {
  this.lists = a;
  // this.bArray = this.getBList(this.aArray); // Now `this.aArray` will have some value provided `someService.get()` returns something
});


// var k = 0, p = 0;
// for (var i = 0; i < this.lists.length; i++) {
//   this.list1[k++] = this.lists[i].name;
// }
// this.filters = [...this.list1];
// this.filtername = [...this.list1];

// console.log("lists"+this.lists)





  }
  k=0;
  count=0;
  once()
  {
    if(this.count==1){
    this.lists = this.auth.namedata;
    
    for (var i = 0; i < this.lists.length; i++) {
      this.list1[this.k++] = this.lists[i].name;
    }
    this.filters = [...this.list1];
    this.filtername = [...this.list1];
  }
  }
  onsearch() {
    this.count++;
    this.once();
    this.imgshow = true;
    this.imgshow1 = false;
    this.imgshow2 = false;


    this.filters = this.list1.filter(item => item.indexOf(this.ser) > -1);

    this.filters = this.filters.map(res => "assets/media/" + res);


  }
  getmettaa() {
    this.registerservice.getmetadata().subscribe(res1 => {
      this.lists = res1;
    });
    // var k = 0, p = 0;

    // // this.lists = this.auth.namedata;
    //  for (var i = 0; i <   this.lists.length; i++) {
    //   this.list1[k++] = this.lists[i].name;
    // }
  }

  checkBox = false;
  re() {

    this.imgshow1 = true;
    this.imgshow = false;
    this.imgshow2 = false;
    this.myArray = [];
    this.myArray1 = [];

    this.myArray2 = [];

    // console.log(this.checkBox)
    this.checkBox = !this.checkBox;
    this.myFrom.reset();
    this.ser="";
  }
  catclos() {
    this.imgshow1 = true;
    this.imgshow = false;
    this.imgshow2 = false;
    this.myArray = [];
    this.checkBox = !this.checkBox;
    this.myFrom.reset();
  }
  catclos1() {
    this.imgshow1 = true;
    this.imgshow = false;
    this.imgshow2 = false;
    this.myArray1 = [];
    this.checkBox = !this.checkBox;
    this.myFrom.reset();
  }
  catclos2() {
    this.imgshow1 = true;
    this.imgshow = false;
    this.imgshow2 = false;
    this.myArray2 = [];
    this.checkBox = !this.checkBox;
    this.myFrom.reset();
  }
  tempFunc(i: String) {
    return i.split('/')[2];
  }
 




  op(v) {
    // alert(v);
    // this.router.navigate[('/upload')];
  }
  toggleShow() {
    this.show = !this.show;
  }

  c = this.auth.currentmail;
  toggle1() {
    this.show1 = !this.show1;

  }
  toggle2() {
    this.show2 = !this.show2;

  }
  toggle3() {
    this.show3 = !this.show3;

  }
  toggle4() {
    this.show4 = !this.show4;

  }
  currmail = this.auth.currentmail;

  getdata() {
    this.registerservice.getmetadata().subscribe(res => {
      this.metdat = res;
    });

  }
  varlog
  ngOnInit() {

    this.getdata();
    this.filters = [...this.list1];
    // console.log(this.metdat)
    // this.lists = this.metdat;
  this.lists=this.auth.getmett1();
  this.varlog=this.cook.get('login');

  if(this.cook.get('login')!="")
  {
    this.router.navigate(['/layout'])
  }
  else{
    this.router.navigate(['/login'])

  }








  }

  logoutt() {
    localStorage.clear();
    sessionStorage.clear();
    this.router.navigate(['/login']);

  }
  isChecked: any;
  newar: any = [];
  fil: String[] = [];
  newar1: any;
  o: number = 10;
  z: any;
  x: any;
  index: number;
  a = 0;
  myArray: String[] = [];
  myArray1: String[] = [];
  myArray2: String[] = [];
  in2
  in1
  in0

  filterForeCasts2(filterVal: any)
  {
    if (this.myArray2.indexOf(filterVal) >= 0) {
        // alert(filterVal);
      this.index = this.myArray2.indexOf(filterVal);

      if (this.index !== -1) {

        this.myArray2.splice(this.index, 1);
      }
    }
    else {

      this.myArray2.push(filterVal);
    }
    this.imgshow = false;
    this.imgshow1 = false;
    this.imgshow2 = true;
    // this.filt = [];
    this.fil = [];
    


    for (var f = 0; f < this.myArray2.length; f++) {
      for (var t = 0; t < this.metdat.length; t++) {
        // alert(this.metdat[t].Brand);

        if (this.myArray2[f] == this.metdat[t].Brand) {
          
        //   if (this.filt.indexOf(this.metdat[t].Filename) >= 0) {
        //     // alert(filterVal);
        //   this.in2 = this.filt.indexOf(this.metdat[t].Filename);
    
        //   if (this.in2 !== -1) {
    
        //     this.filt.splice(this.in2, 1);
        //   }
        // }
        // else {
    
        //   this.filt.push(this.metdat[t].Filename);
        // }



           this.filt.push(this.metdat[t].Filename);
        }
      }
    }
     
  }


  filterForeCasts1(filterVal: any)
  {
    if (this.myArray1.indexOf(filterVal) >= 0) {
        // alert(filterVal);
      this.index = this.myArray1.indexOf(filterVal);
      // alert(this.index);

      if (this.index !== -1) {

        this.myArray1.splice(this.index, 1);
      }
    }
    else {
      // alert("push")
      this.myArray1.push(filterVal);
    }



    this.imgshow = false;
    this.imgshow1 = false;
    this.imgshow2 = true;
    this.filt = [];
    this.fil = [];
    


    for (var f = 0; f < this.myArray1.length; f++) {
      for (var t = 0; t < this.metdat.length; t++) {
         console.log(this.metdat[t].BusinessSegment);
        if (this.myArray1[f] == this.metdat[t].BusinessSegment) {
          this.filt.push(this.metdat[t].Filename);
        }
      }
    }
     
  }

  filterForeCasts(filterVal: any) {
    // this.o = 1

    if (this.myArray.indexOf(filterVal) >= 0) {
      // alert(filterVal);

      this.index = this.myArray.indexOf(filterVal);
      // alert(this.index);
      if (this.index !== -1) {

        this.myArray.splice(this.index, 1);
      }
    }
    else {
      // alert("in else");
      this.myArray.push(filterVal);
    }

  
    this.imgshow = false;
    this.imgshow1 = false;
    this.imgshow2 = true;
    this.filt = [];
    this.fil = [];
    

    for (var f = 0; f < this.myArray.length; f++) {
      for (var t = 0; t < this.metdat.length; t++) {
        if (this.myArray[f] == this.metdat[t].Country) {
          this.filt.push(this.metdat[t].Filename);
        }
      }
    }
            

  }


  filterForeCastsa(filterVal: any) {
    
            
    alert(filterVal)
  }

  v: String;

  check(r: string): String {
    this.v = r.split(".")[1];
    if (this.v === "jpg" || this.v === "png")
      return "imgg";
    else if (this.v === "mp4")
      return "vid";
    else {
      return "pdf";
    }
  }
  cut(r): String {
    return r.split('/')[2];
  }
  close(sh) {
    this.filterForeCasts(sh);
  }
  
  close1(sh) {
    this.filterForeCasts1(sh);
  }
  close2(sh) { 
    this.filterForeCasts2(sh);
  }
  closea(sh)
  {
    this.filterForeCastsa(sh);

  }
  checklen() {
    if (this.o == 10) {
      return true;
    }
    else {
      return false;
    }
  }

value:any;
  onChange(topic: string) {
    let index = this.myArray.indexOf(topic);
    if (index == -1) {
      this.myArray.push(topic);
    
    } else {
      this.myArray.splice(index, 1);
    }
    this.imgshow = false;
    this.imgshow1 = false;
    this.imgshow2 = true;
    this.filt = [];
    this.fil = [];   

    for (var f = 0; f < this.myArray.length; f++) {
      for (var t = 0; t < this.metdat.length; t++) {
        if (this.myArray[f] == this.metdat[t].Country) {
          this.filt.push(this.metdat[t].Filename);
        }
      }
    }
  }


  isSelected(topic) {
    return this.myArray.indexOf(topic) >= 0;
  }
  onChange1(topic: string) {

    let index = this.myArray1.indexOf(topic);
    if (index == -1) {

      this.myArray1.push(topic);
    
    } else {
      this.myArray1.splice(index, 1);
    }
    this.imgshow = false;
    this.imgshow1 = false;
    this.imgshow2 = true;
    this.filt = [];
    this.fil = [];
    
    for (var f = 0; f < this.myArray1.length; f++) {
      for (var t = 0; t < this.metdat.length; t++) {
        if (this.myArray1[f] == this.metdat[t].BusinessSegment) {
          this.filt.push(this.metdat[t].Filename);
        }
      }
     }
  }

  isSelected1(topic) {
    // console.log(topic)
    return this.myArray1.indexOf(topic) >= 0;
  }

  onChange2(topic: string) {
    let index = this.myArray2.indexOf(topic);
    if (index == -1) {
      this.myArray2.push(topic); 
    } else {
      this.myArray2.splice(index, 1);
    }
    this.imgshow = false;
    this.imgshow1 = false;
    this.imgshow2 = true;
    this.filt = [];
    this.fil = [];
    
    for (var f = 0; f < this.myArray2.length; f++) {
      for (var t = 0; t < this.metdat.length; t++) {
        if (this.myArray2[f] == this.metdat[t].Brand) {
          this.filt.push(this.metdat[t].Filename);
        }
      }
    }




  }
  isSelected2(topic) {
    // console.log(topic)
    return this.myArray2.indexOf(topic) >= 0;
  }
  func(Filename){
    return Filename.split('/').join('-');
  }



ext:string
asset:String[]=["Image","Video","PDF"];
myArray3:String[];

  onChangea(topic: string) {
      
    for (var t = 0; t < this.metdat.length; t++)
    {
      this.ext=this.metdat.name.split('.')[1];
      if(this.ext=="jpg"||this.ext=="png" && topic=="Image")
      {
        this.filt.push(this.metdat.Filename);
      }
      else if(this.ext=="mp4" && topic=="Video")
      {
        this.filt.push(this.metdat.Filename);

      }
      else
      {
        this.filt.push(this.metdat.Filename);

      }
    }

  }


  isSelecteda(topic) {
    return this.filt.indexOf(topic) >= 0;
    // alert()

  }
  onlogout(){
 this.cook.delete('login');

    this.auth.logout();
  }
}

