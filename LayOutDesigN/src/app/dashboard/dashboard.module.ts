import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './components/layout/layout.component';
import { AppRoutingModule } from '../app-routing.module';
import { FileuploadComponent } from './components/fileupload/fileupload.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NewwindowComponent } from './components/newwindow/newwindow.component';

// import { CookieService } from 'ngx-cookie-service';

@NgModule({
  imports: [
    CommonModule,
  AppRoutingModule,
  FormsModule,
  ReactiveFormsModule,
  // CookieService

  ],
  exports:[
    LayoutComponent,
    NewwindowComponent
  ],
  declarations: [LayoutComponent, FileuploadComponent, NewwindowComponent]
})
export class DashboardModule { }
