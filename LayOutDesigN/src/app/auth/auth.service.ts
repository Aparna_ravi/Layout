import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import IUser from '../models/user';
import  IFile from '../models/Fileup';

import { Subject } from 'rxjs';
import { RegisterService } from '../register.service';
// import { CookieService } from '../../../node_modules/ngx-cookie-service';
import { CookieService } from 'ngx-cookie-service';



@Injectable({
  providedIn: 'root'
})
export class AuthService {
  logg:boolean
   mail:string
  namedata: IFile ;
  currentmail:any;
   us:any;
   log:boolean=false;
   test1:Array<String>=[];
  constructor(private router :Router,private reg:RegisterService, private cookieService:CookieService) {
  this.getmett();

 this.getdata();

  }
  getmett1() {
    this.reg.getmetadata().subscribe(res1 => {
      this. namedata = res1;
    });
    return this.namedata;

  }
  getmett() {
    this.reg.getmetadata().subscribe(res1 => {
      this. namedata = res1;
      console.log(this.namedata)
    });
    // var k=0;
    // for(var i=0;i<6;i++)
    // {
    //  this.test1[k++]=this.namedata[i].name;
    // //  console.log(this.list1[p++]);
    // }
// console.log(this.namedata)

  }
  getdata() {
    this.reg.getCoins().subscribe(res => {
      this.us = res;
    });
  }
  public loginChangeEvent :Subject<string> =new Subject<string>();
 

  login(userInfo:IUser):boolean
  {
    console.log(this.us);

this.mail=userInfo.email;
  
const res =this.us.find((u)=>{
  return u.Email=== userInfo.email && u.Password===userInfo.password;
});
if(res)
{
  this.currentmail=userInfo.email;
   sessionStorage.setItem('userEmail',userInfo.email);
   this.cookieService.set( 'login',this.currentmail);
  //  this.cookieValue = this.cookieService.get('Test');
  //  this.cookieService.set( 'Test', this.currentmail );


  
  // this.logg=true;

  this.router.navigate(['/layout']);
 this.loginChangeEvent.next('loggedIn');
  return true;
}


this.log=false;
console.log("login failed");
this.logg=false;
return false;
  }
  isLoggedIn()
  {
    console.log("loggedin")
    const userEmail=sessionStorage.getItem('userEmail');
    return userEmail?true :false;
  }
  logout()
  {
    
sessionStorage.clear();
 this.cookieService.delete('login');
this.router.navigate(['/login'])
  }
}
