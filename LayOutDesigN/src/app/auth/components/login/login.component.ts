import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../auth.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http'
import { RegisterService } from '../../../register.service';
// import { CookieService } from '../../../../../node_modules/ngx-cookie-service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
logs:boolean
user:any;
  constructor(private service:AuthService,
  private router :Router,private http: HttpClient, private cookieService:CookieService) {

    if(this.service.isLoggedIn())
    {
    console.log("inside if")
     
    }
    
    else
    {
      this.router.navigate(["/login"]);
    console.log("inside else")
    }
   }
  onLoginSubmit(myForm:NgForm)
  {
    if(myForm.valid)
    {
    this.service.login(myForm.value);
    }
    else{
    this.logs=this.service.log;

      console.log(myForm.value);
      console.log("error");

    }
  }
  ngOnInit() {
  //  if(this.cookieService.get('login')!=null)
  //   {
  //     this.router.navigate(['/layout']);
  //   } 
    // else{
    //    this.router.navigate(['/login']);

    // }
  
  }
  
setlog()
{
  this.logs=this.service.log;

}
  register()
{
  console.log("register")
  this.router.navigate(['/reg']);
}
}
