var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Items
var Filedata= new Schema({
    Country: {
    type: String
  },
  BusinessSegment: {
    type: String
  },
  Brand: {
    type: String
  },
  Usermail: {
    type: String
  },
  Filename: {
    type: String
  },
  name: {
    type: String
  },
 
},{
    collection: 'Filedata'
});

module.exports = mongoose.model('Filedata', Filedata);